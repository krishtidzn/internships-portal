﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using PraksiProject.Models;

namespace PraksiProject.Controllers
{
    public class AccountController : Controller
    {

        /// <summary>
        /// GET funkcija koja go povikuva pogledot za najavuvanje na korisnik
        /// </summary>
        /// <returns></returns>

        public ActionResult LogOn()
        {
            return View();
        }
        /// <summary>
        /// Funkcija koja go povikuva pogledot za najavuvanje na administrator
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOnAdmin()
        {
            return View();
        }

        /// <summary>
        /// Post metod koj gi zema podatocite koi gi vnesil korisnikot, gi provretuva vo bazata i ako e se vo red
        /// go prenasocuva kon stanata so zafateni praksi.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Zafateni");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Погрешно корисничко име или лозинка.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Post metod koj gi zema podatocite koi bile vneseni od administratorot i ako se vo red
        /// se prenasocuva kon stanata so praksite so privilegii na administrator
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LogOnAdmin(LogOnModel model, string returnUrl)
        {
            if ((model.Password == "admin123") && (model.UserName == "admin"))
            {
                return RedirectToAction("Index","Praksi");
            }
            else {
                return RedirectToAction("LogOn", "Account");
            }

            // If we got this far, something failed, redisplay form
           
        }
        /// <summary>
        /// Get metod za odjava koja go vraka korisnikot na pocetnata strana
        /// </summary>
        /// <returns></returns>

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("LogOn", "Account");
        }

        
        /// <summary>
        /// GET funkcija koga go povikuva pogledot za registracija na nov korisnik
        /// </summary>
        /// <returns></returns>

        public ActionResult Register()
        {
            return View();
        }

        
        /// <summary>
        /// Post metod koj podatocite vneseni vo formata za registracija gi zapisuva vo bazata za korisnici
        /// i go prenasocuva noviot korisnik kon stranata za izbor na praksa
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("IndexUser", "Praksi");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// GET metod koj go povikuva pogledot za promena na lozinka na korisnikot
        /// </summary>
        /// <returns></returns>

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Post metod koj novata lozinka ja zapisuva vo bazata so korisnici
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "Моменталната лозинка е погрешна или новата не е валидна.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// GET funkcija koja se povikuva pri uspesna promena na lozinkata
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
