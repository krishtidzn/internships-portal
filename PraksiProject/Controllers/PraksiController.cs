﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PraksiProject.Models;

namespace PraksiProject.Controllers
{ 
    public class PraksiController : Controller
    {
        private PraksiDBContext db = new PraksiDBContext();
        
        ///GET Metodi:
        
        /// <summary>
        /// Za povikuvanje na pogled so site praksi so privilegii na administrator.
        /// Kako argument se ispraka tabelata so praksite
        /// </summary>
        /// <returns></returns>
        

        public ViewResult Index()
        {
            return View(db.Praksi.ToList());
        }

        /// <summary>
        ///  Za povikuvanje na pogled so site praksi so ograniceni privilegii.
        /// Kako argument se ispraka tabelata so praksite
        /// </summary>
        /// <returns></returns>
        public ViewResult IndexUser()
        {
            return View(db.Praksi.ToList());
        }

        /// <summary>
        /// Za povikuvanje na pogled so detali za praksa kade kako argument se ispraka identifikaciski
        /// broj na selektiranata praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ViewResult Details(int id)
        {
            Praksi praksi = db.Praksi.Find(id);
            return View(praksi);
        }

        /// <summary>
        /// Za povikuvanje na pogled za dodavanje nova praksa vo tabelata
        /// </summary>
        /// <returns></returns>

        public ActionResult Create()
        {
            return View();
        }
        
        /// <summary>
        /// Za povikuvanje na pogled za brisenje na praksa, vlezen argument id na praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Delete(int id)
        {
            Praksi praksi = db.Praksi.Find(id);
            return View(praksi);
        }

      
        /// <summary>
        /// Za povikuvanje na pogled za promena na podatoci za postoecka praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            Praksi praksi = db.Praksi.Find(id);
            return View(praksi);
        }

        ///POST metodi:

        /// <summary>
        /// Za dodavanje nova praksa vo bazata, po sto se prenasocuva kon listata so site praksi so privilegii
        /// na administrator
        /// </summary>
        /// <param name="praksi"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Create(Praksi praksi)
        {
            if (ModelState.IsValid)
            {
                db.Praksi.Add(praksi);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(praksi);
        }
        
       

        /// <summary>
        /// Funkcija za promena na podatoci za praksa, i vrakanje na lsitata so site praksi
        /// </summary>
        /// <param name="praksi"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(Praksi praksi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(praksi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(praksi);
        }

       

        /// <summary>
        /// Za brisenje na praksa kade kako argument se prosleduva identifikaciskiot broj na praksata
        /// koja treba da se izbrisi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Praksi praksi = db.Praksi.Find(id);
            db.Praksi.Remove(praksi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}