﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PraksiProject.Models;

namespace PraksiProject.Controllers
{ 
    public class ZafateniController : Controller
    {
        private PraksiDBContext db = new PraksiDBContext();

        ///Get metodi:

        /// <summary>
        /// Za povik na pogled so zafateni praksi so privilegii na administrator
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexAdmin()
        {

            return View(db.ZafateniPraksi.ToList());
        }

        /// <summary>
        /// Za povik na pogled so zafateni praksi so ograniceni privilegii
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            return View(db.ZafateniPraksi.ToList());
        }
        
        /// <summary>
        /// Za povik na pogled za brisenje na zafatena praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Delete(int id)
        {
            ZafateniPraksi zafatenipraksi = db.ZafateniPraksi.Find(id);
            return View(zafatenipraksi);
        }

        /// <summary>
        /// Za povik na pogled so detali za zafatena praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ViewResult Details(int id)
        {
            ZafateniPraksi zafatenipraksi = db.ZafateniPraksi.Find(id);
            return View(zafatenipraksi);
        }
        
        /// <summary>
        /// Za povik na pogled za promena na postoecka zafatena praksa, kako vlezen argument se prosleduva
        /// id na zafatena praksa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //public ActionResult Edit(int id)
        //{
        //    ZafateniPraksi zafatenipraksi = db.ZafateniPraksi.Find(id);
        //    return View(zafatenipraksi);
        //}
        
        /// <summary>
        /// Za povik na pogled za dodavanje na nova zafatena praksa
        /// </summary>
        /// <returns></returns>

        public ActionResult Create()
        {
            return View();
        } 

        ///POST metodi:

        /// <summary>
        /// Za  dodavanje na nova zafatena praksa vo bazata, i prenasocuvanje kon lsita so site afateni praksi
        /// bez privilegii na administrator
        /// </summary>
        /// <param name="zafatenipraksi"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Create(ZafateniPraksi zafatenipraksi)
        {
            if (ModelState.IsValid)
            {
                db.ZafateniPraksi.Add(zafatenipraksi);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(zafatenipraksi);
        }

        /// <summary>
        /// Za popolnuvanje na podatocite za korisnikot koj ja zafaka praksata so identifikacionen broj id,
        /// koj e prosleden na vlez, i nejzino zapisuvanje vo bazata so zafateni praksi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ZafatiPraksa(int id)
        {
            LogOnModel model= new LogOnModel();
            ZafateniPraksi praksa = new ZafateniPraksi();
            if (ModelState.IsValid)
            {
                
                praksa.IDPraksa = id;
                db.ZafateniPraksi.Add(praksa);
                db.SaveChanges();
                return RedirectToAction("Edit", new {id=praksa.ID});
            }

            return View(praksa);
        }
       

        /// <summary>
        /// Za promena na podatocite na postoecka zafatena praksa 
        /// </summary>
        /// <param name="zafatenipraksi"></param>
        /// <returns></returns>

        //[HttpPost]
        //public ActionResult Edit(ZafateniPraksi zafatenipraksi)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(zafatenipraksi).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(zafatenipraksi);
        //}

        
        /// <summary>
        /// Za brisenje na postoecka zafatena praksa, so identifikaciski broj id koj se prima kako vlezen argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ZafateniPraksi zafatenipraksi = db.ZafateniPraksi.Find(id);
            db.ZafateniPraksi.Remove(zafatenipraksi);
            db.SaveChanges();
            return RedirectToAction("IndexAdmin");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}