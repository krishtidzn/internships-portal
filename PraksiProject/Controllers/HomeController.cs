﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PraksiProject.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// GET: funkcija koja se povikuva pri startuvanje na proektot i se otvara pogledot za najava na korisnik
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //ViewBag.Message = "Welcome to ASP.NET MVC!";

            return RedirectToAction("LogOn", "Account");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
