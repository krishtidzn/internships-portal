﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PraksiProject.Models
{
    ///Gotov model za najava i registracija na korisnici
    ///pri sto se generira baza so korisnicko ime i lozinka

    /// <summary>
    /// Funkcija za promena na lozinka
    /// </summary>
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Моментална лозинка")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Лозинката мора да има барем 6 карактери.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Нова лозинка")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потврда на нова лозинка")]
        [Compare("NewPassword", ErrorMessage = "Двете лозинки се совпаѓаат")]
        public string ConfirmPassword { get; set; }
    }
    /// <summary>
    /// Funkcija za Najava na korisnik
    /// </summary>
    public class LogOnModel
    {
        [Required]
        [Display(Name = "Корисничко име")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }

        [Display(Name = "Запомни ме?")]
        public bool RememberMe { get; set; }
    }
    /// <summary>
    /// Funkcija za registracija na nov korisnik
    /// </summary>
    public class RegisterModel
    {
        [Required]
        [Display(Name = "Корисничко име")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Emai адреса")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Лозинката мора да има барем 6 карактери.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Лозинка")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потврда на нова лозинка")]
        [Compare("Password", ErrorMessage = "Двете лозинки не се совпаѓаат.")]
        public string ConfirmPassword { get; set; }
    }
}
