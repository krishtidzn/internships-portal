﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PraksiProject.Models
{
    /// <summary>
    /// Modeel za praksi koj generira dve tabeli. Vo prvata se cuvaat podatoci za praksata,
    /// a vo vtorata za studentot koj ja zafatil praksata.
    /// </summary>
    public class Praksi
    {
        public int ID { get; set; }
        public string Drzava { get; set; }
        public string Grad { get; set; }
        public string Oblast { get; set; }
        public int Vremetraenje { get; set; } 
    }

    public class ZafateniPraksi
    {
        public int ID { get; set; }
        public string ImeStudent { get; set; }
        public string PrezimeStudent { get; set; }
        public int IDPraksa { get; set; }

    }

    public class PraksiDBContext : DbContext
    {
        public DbSet<Praksi> Praksi { get; set; }
        public DbSet<ZafateniPraksi> ZafateniPraksi { get; set; }
    
    }
}